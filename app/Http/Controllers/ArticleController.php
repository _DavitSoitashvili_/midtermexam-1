<?php

namespace App\Http\Controllers;

use App\Mail\ArticleEdited;
use App\Models\Article;
use App\Models\User;
use App\Notifications\ArticleLiked;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Mail;

class ArticleController extends BaseController
{
    function index()
    {
        $articles = Article::query()
            ->where("is_published", 1)->orderBy("likes", "desc")->paginate(15);
        return View("show_articles", compact("articles"));
    }

    function edit($id)
    {
        $article = Article::findOrFail($id);
        return view("edit_article", compact("article"));
    }

    function storeEdited(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $this->formValidation($request);
        $article = Article::findOrFail($id);
        $article->update($request->all());
        $this->sendUpdatedArticleMessage($article);
        $this->sendArticleEditedTemplateMessage($article);
        return redirect()->action([ArticleController::class, 'index']);
    }

    private function sendUpdatedArticleMessage(Article $article)
    {
        Mail::raw($article->body, function ($message) {
            $message->to("articles@gmail.com");
        });
    }

    private function sendArticleEditedTemplateMessage(Article $article) {
        Mail::to($article->body)->send(new ArticleEdited());
        echo "Message about edit article has been sent!";
    }

    public function like($id): \Illuminate\Http\RedirectResponse
    {
        $article = Article::findOrFail($id);
        if ($article) {
            $article->likes += 1;
            $article->save();
            $this->postLikedNotification($article);
        }

        return redirect()->action([ArticleController::class, 'index']);
    }

    private function postLikedNotification(Article $article) {
        $user = User::find(1);
        $data = [
            $article->title." has been liked"
        ];
        $user->notify(new ArticleLiked($data));
    }

    public function delete($id): \Illuminate\Http\RedirectResponse
    {
        $article = Article::findOrFail($id);
        $article->delete();
        return redirect()->action([ArticleController::class, 'index']);
    }

    private function formValidation(Request $request)
    {
        $request->validate([
            "title" => ['required'],
            "body" => ['required'],
            "author" => ['required']
        ]);
    }
}
