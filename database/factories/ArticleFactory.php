<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \App\Models\Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name(),
            'body' => $this->faker->paragraph(),
            'author' => $this->faker->name(),
            'likes' => $this->faker->numberBetween(0,20),
            'is_published' => $this->faker->boolean()
        ];

    }
}
