@extends("layout")
@section("Title")
    Edit Article
@endsection
@section("Content")
    <form method="post" action="{{route("article.store.edited", $article->id)}}">
        @csrf
        @method("PUT")
        <div class="mb-3">
            <label class="form-label">Title</label>
            <input name="title" type="text" class="form-control" value="{{$article->title}}"
                   aria-describedby="emailHelp">
            <span class="alert-danger">{{$errors->first("title")}}</span>
        </div>
        <div class="mb-3">
            <label class="form-label">Text Body</label>
            <input name="body" type="text" class="form-control" value="{{$article->body}}"
                   aria-describedby="emailHelp">
            <span class="alert-danger">{{$errors->first("body")}}</span>
        </div>
        <div class="mb-3">
            <label class="form-label">Author</label>
            <input name="author" type="text" class="form-control" value="{{$article->author}}"
                   aria-describedby="emailHelp">
            <span class="alert-danger">{{$errors->first("author")}}</span>
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
@endsection
