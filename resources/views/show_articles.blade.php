@extends("layout")
@section("Title")
    Show Articles
@endsection
@section("Content")
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Title</th>
            <th scope="col">Body Text</th>
            <th scope="col">Author</th>
            <th scope="col">Likes</th>
            <th scope="col">IsPublished</th>
        </tr>
        </thead>
        <tbody>
        @foreach($articles as $article)
                <td style="font-size: 18px">{{$article->id}}
                    <div style="display: flex">
                        <form method="get" action="{{route("article.edit", $article->id)}}">
                            <button style="width:50px" class="btn btn-success" type="submit">Edit</button>
                        </form>
                        <form method="post" action="{{route("article.like", $article->id)}}">
                            @csrf
                            @method("PUT")
                            <button style="width:50px; margin-left: 5px" class="btn btn-primary" type="submit">Like</button>
                        </form>
                        <form method="post" action="{{route("article.delete", $article->id)}}">
                            @csrf
                            @method("DELETE")
                            <button style="width:50px; margin-left: 5px" class="btn btn-danger" type="submit">-</button>
                        </form>
                    </div></td>
                <td style="font-size: 18px">{{$article->title}}</td>
                <td style="font-size: 18px">{{$article->body}}</td>
                <td style="font-size: 18px">{{$article->author}}</td>
                <td style="font-size: 18px">{{$article->likes}}</td>
                <td style="font-size: 18px">+</td>
                </tr>
        @endforeach
        </tbody>
    </table>
    {{$articles->links()}}
@endsection
